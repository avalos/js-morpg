var createGame = require('voxel-engine');
var highlight = require('voxel-highlight');
var terrain = require('voxel-perlin-terrain')
var player = require('voxel-player');
var voxel = require('voxel');
var extend = require('extend');
// var fly = require('voxel-fly');
var walk = require('voxel-walk');
var texturePath = "/textures/";

module.exports = function(opts, setup) {
  setup = setup || defaultSetup;

  var defaults = {
    generateChunks: false,
    // generate: voxel.generator['Valley'],
    chunkDistance: 2,
    // materials: ['#fff', '#000'],
    // materialFlatColor: true,
    worldOrigin: [0, 0, 0],
    controls: {
      discreteFire: true
    },
    texturePath: texturePath,
    materials: [
      ['grass', 'dirt', 'grass_dirt'],
      'brick',
      'crate',
      'diamond',
      'obsidian'
    ],
    lightsDisabled: true // Sky
  };

  opts = extend({}, defaults, opts || {});

  // setup the game and add some trees
  var game = window.game = createGame(opts);
  var container = opts.container || document.body;
  window.game = game; // for debugging
  game.appendTo(container);
  if (game.notCapable()) return game;

  var createPlayer = player(game);

  // create the player from a minecraft skin file and tell the
  // game to use it as the main player
  var avatar = createPlayer(opts.playerSkin || 'player.png');
  avatar.possess();
  avatar.yaw.position.set(2, 14, 4);

  // Setup Sky
  game.createSky = require('voxel-sky')(game);

  setup(game, avatar);

  // Generate world (on Missing Chunk)
  var generator = terrain('helloworld', 0, 5);
  var chunkSize = 32;

  var generatedChunks = 0;
  game.voxels.on('missingChunk', function(p) {
    if(generatedChunks > 64) return;

    var voxels = generator(p, chunkSize);
    var chunk = {
      position: p,
      dims: [chunkSize, chunkSize, chunkSize],
      voxels: voxels
    };
    game.showChunk(chunk);
    generatedChunks++;
  });

  return game;
}

function defaultSetup(game, avatar) {

  var target = game.controls.target();
  // var makeFly = fly(game);
  // game.flyer = makeFly(target);

  // highlight blocks when you look at them, hold <Ctrl> for block placement
  var blockPosPlace, blockPosErase;
  var hl = game.highlighter = highlight(game, { color: 0xffffff });
  hl.on('highlight', function (voxelPos) { blockPosErase = voxelPos });
  hl.on('remove', function (voxelPos) { blockPosErase = null });
  hl.on('highlight-adjacent', function (voxelPos) { blockPosPlace = voxelPos });
  hl.on('remove-adjacent', function (voxelPos) { blockPosPlace = null });

  // toggle between first and third person modes
  window.addEventListener('keydown', function (ev) {
    if (ev.keyCode === 'R'.charCodeAt(0)) avatar.toggle();
  });

  // block interaction stuff, uses highlight data
  var currentMaterial = 1;

  game.on('fire', function (target, state) {
    var position = blockPosPlace;
    if (position) {
      game.createBlock(position, currentMaterial);
    }
    else {
      position = blockPosErase;
      if (position) game.setBlock(position, 0);
    }
  });

  var sky = game.createSky(1200);

  var gameSky;
  game.on('tick', function(dt) {
    gameSky = sky(dt);

    walk.render(target.playerSkin);
    var vx = Math.abs(target.velocity.x);
    var vz = Math.abs(target.velocity.z);
    if (vx > 0.001 || vz > 0.001) walk.stopWalking();
    else walk.startWalking();

    if(!gameSky.initialized){
      gameSky.speed(0.05);
      gameSky.initialized = true;
    }
  });

  var selectMaterial = function(material) {
    $('#toolbar .block').removeClass('selected');
    $("#toolbar .block[material='" + material + "']").addClass('selected');

    switch (material) {
      case 'grass':
        currentMaterial = 1;
      break;
      case 'brick':
        currentMaterial = 2;
      break;
      case 'crate':
        currentMaterial = 3;
      break;
      case 'diamond':
        currentMaterial = 4;
      break;
      case 'obsidian':
        currentMaterial = 5;
      break;
      default:
      break;
    }
  }

  window.addEventListener('keyup', function(e){
    if(e.keyCode === 49) {
      selectMaterial('grass');
    } else if (e.keyCode === 50) {
      selectMaterial('brick');
    } else if (e.keyCode === 51) {
      selectMaterial('crate');
    } else if (e.keyCode === 52) {
      selectMaterial('diamond');
    } else if (e.keyCode === 53) {
      selectMaterial('obsidian');
    }
  });

  $(function(){
    var toolbarWidth = ($('#toolbar .block').length + 1) * 32 + 4;
    $('#toolbar').css({ marginLeft: '-' + ($('#toolbar').width()/2) + 'px', width: toolbarWidth });
  });

}
